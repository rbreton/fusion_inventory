#!/bin/sh -x
#
# Ejecutar como root:
#     su soporte
#     sudo su
#
HOSTNAME=`hostname`
DATE=`date +"%Y-%d-%m-%H:%M"`
IP_ADDRESS=` /sbin/ifconfig | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | sort | awk '{print $1}'`
OUTPUT=${HOSTNAME}_${IP_ADDRESS}_${DATE}.hw.log
echo "#######################################################" | tee -a ${OUTPUT}
echo "Generando fichero ${OUTPUT}" |tee -a ${OUTPUT}
echo "#######################################################" | tee -a ${OUTPUT}
echo ${DATE} | tee -a ${OUTPUT}
echo ${HOSTNAME} |tee -a ${OUTPUT}
echo "# uname -a" | tee -a ${OUTPUT}
uname -a  |tee -a ${OUTPUT}
echo "#######################################################" | tee -a ${OUTPUT}
echo "# head -n1 /etc/issue"
head -n1 /etc/issue  | tee -a ${OUTPUT}
echo "# lsb_release -a"
lsb_release -a  | tee -a ${OUTPUT}
echo "#######################################################" | tee -a ${OUTPUT}
echo "# cat /proc/partitions" | tee -a ${OUTPUT}
cat /proc/partitions  |tee -a ${OUTPUT}
grep MemTotal /proc/meminfo  |tee -a ${OUTPUT}
echo "#######################################################" | tee -a ${OUTPUT}
echo "#grep \"model name\" /proc/cpuinfo"  |tee -a ${OUTPUT}
grep "model name" /proc/cpuinfo  |tee -a ${OUTPUT}
echo "#######################################################" | tee -a ${OUTPUT}
echo "#hdparm -i /dev/sda" | tee -a ${OUTPUT}
hdparm -i /dev/sda | tee -a ${OUTPUT}
echo "#######################################################" | tee -a ${OUTPUT}
echo "#grep MemTotal /proc/meminfo" |tee -a ${OUTPUT}
grep MemTotal /proc/meminfo |tee -a ${OUTPUT}





echo "#######################################################" | tee -a ${OUTPUT}
OUTPUT_LSHW=${HOSTNAME}_${IP_ADDRESS}_${DATE}.lshw.log
dpkg-query -l lshw
if [ $? -eq 0 ]; then
echo "Generando fichero ${OUTPUT_LSHW}" | tee -a ${OUTPUT}
lshw | tee ${OUTPUT_LSHW}
else
apt-get install lshw
echo "Generando fichero ${OUTPUT_LSHW}" | tee -a ${OUTPUT}
lshw | tee ${OUTPUT_LSHW}
fi


echo "#######################################################" | tee -a ${OUTPUT}
OUTPUT_FI=${HOSTNAME}_${IP_ADDRESS}_${DATE}.html
dpkg-query -l  fusioninventory-agent
if [ $? -eq 0 ]; then
		echo "Encontrado fusioninventory-agent" | tee -a ${OUTPUT}
        echo "Generando fichero ${OUTPUT_FI}" | tee -a ${OUTPUT}
        fusioninventory-agent --local ${OUTPUT_FI} --html
fi
