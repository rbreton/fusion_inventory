#!/bin/sh
#
# Ejecutar como root:
#     su soporte
#     sudo su
#

# Actualización repositorios
apt-get update
# Necesario para averiguar la versión de Debian/Ubuntu/Guadalinex que tenemos
apt-get install lsb-release
# Añadimos repositorio
echo "deb http://debian.fusioninventory.org/debian/ `lsb_release -cs` main" >> /etc/apt/sources.list
# Descargamos la clave pública y la añadimos al repostiorio
wget -O - http://debian.fusioninventory.org/debian/archive.key | sudo apt-key add -
apt-get update
# Instalamos fusioninventory
apt-get install fusioninventory-agent libcrypt-ssleay-perl

# Añadimos línea de configuración al comiendo de agent.cfg
sed -i "1i\server=http://svrocsO1/glpi/plugins/fusioninventory/,http://svrocsO1/ocsinventory" /etc/fusioninventory/agent.cfg

# Añadimos MODE=daemon a fusioninventory-agent
if [ -f  /etc/default/fusioninventory-agent ]; then
        fgrep 'MODE=daemon' /etc/default/fusioninventory-agent
	if [ $? -eq 0 ]; then
		echo "Encontrado MODE=daemon"
		sed -i '/MODE=/c\MODE=daemon' /etc/default/fusioninventory-agent
	else
		echo "NO Encontrado MODE=daemon"
		echo "MODE=daemon" >> /etc/default/fusioninventory-agent
	fi
else
	echo "No existe fichero"
	echo "MODE=daemon" > /etc/default/fusioninventory-agent
fi

# Arrancamos demonio
fusioninventory-agent -f

